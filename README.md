# Shaking Incubator

Most biology labs in Cameroon and elsewhere in resource-constrained contexts are under-equipped due to the high cost of lab equipment and it is essential for many protocols including open enzyme manufacturing to have optimal growth temperature for important microrganisms such as Escherichia coli bacteria. We believe that DIY and Open Science Hardware can facilitate access to low-cost and high-quality equipment for biology labs in Cameroon, Ghana and beyond. 

This repository hosts files for a Shaking Incubator designed by MboaLab in collaboration with Ongola FabLab.
Funded by Open Bioeconomy Lab and Shuttleworth Foundation, building from a project funded by the Following Demand for Open Science Hardware (FOSH) initiative with funding from Mozilla Foundation.

Copyright 2020  - MboaLab and Open Bioeconomy Lab //
Software released under MIT License //
Hardware released under CERN Open Hardware License v1.2

## Making things better

If you spot something that could be done better or more elegantly, please [raise an issue](https://gitlab.com/open-bioeconomy-lab/shaking-incubator/-/issues) or directly submit a pull request.

